package com.mkravchenko;

import com.mkravchenko.tables.TypeMultiplicationTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.mkravchenko.util.PropertiesUtil.get;

public class App {
    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        log.info("The application is running.");
        try {
            var resultMultiply = TypeMultiplicationTable
                    .getMultiplicationTable(System.getProperty("type", "int"))
                    .getStingResult(get("min"), get("max"), get("increment"));
            log.info("{}", resultMultiply);
            log.info("The application completed successfully.");
        } catch (ExceptionInInitializerError | RuntimeException e) {
            log.error("--Application failure!--", e);
        }
    }
}
