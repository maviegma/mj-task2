package com.mkravchenko.util;

import java.io.InputStreamReader;
import java.util.Properties;

public final class PropertiesUtil {
    private static final Properties PROPERTIES = new Properties();
    private static final String PATH = "app.properties";

    static {
        loadProperties();
    }

    public static String get(String key) {
        return PROPERTIES.getProperty(key);
    }

    private static void loadProperties() {
        try (var stream = PropertiesUtil.class.getClassLoader().getResourceAsStream(PATH)) {
            assert stream != null;
            try (var reader = new InputStreamReader(stream)) {
                PROPERTIES.load(reader);
            }
        } catch (Exception ex) {
            throw new RuntimeException("File '" + PATH + "' not found.");
        }
    }

    private PropertiesUtil() {
    }
}