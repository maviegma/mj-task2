package com.mkravchenko.tables;

import java.util.Objects;
import java.util.Set;

public final class TypeMultiplicationTable {
    private static final Set<String> types = Set.of("byte", "short", "int", "long", "float", "double");

    private TypeMultiplicationTable() {
    }

    public static MultiplicationTable getMultiplicationTable(String type) {
        Objects.requireNonNull(type);
        String typeLoverCase = type.toLowerCase();
        if (types.contains(typeLoverCase)) {
            var multiplicationTable = new MultiplicationTable();
            multiplicationTable.setType(typeLoverCase);
            return multiplicationTable;
        }
        throw new RuntimeException("Incorrect number type name \"" + typeLoverCase
                + "\". Select type byte, short, integer, long, float or double.");
    }
}
