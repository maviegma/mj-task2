package com.mkravchenko.tables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class MultiplicationTable {
    private  String type;
    private static final Logger log = LoggerFactory.getLogger(MultiplicationTable.class);

    public String getStingResult(String min, String max, String inc) {
        Number minimum = getNumber(min);
        Number maximum = getNumber(max);
        Number increment = getNumber(inc);

        return getMultiplicationTableAsString(
                getMultiplicationTable(minimum, maximum, increment));
    }

    private List<List<Number>> getMultiplicationTable(Number min, Number max, Number inc) {
        checkInputValue(min, max, inc);
        double minimum = Math.min(min.doubleValue(), max.doubleValue());
        double maximum = Math.max(min.doubleValue(), max.doubleValue());
        double increment = inc.doubleValue();
        List<Number> rowTable;
        List<List<Number>> multiplicationTable = new ArrayList<>();
        if (increment > 0) {
            for (double num1 = minimum; num1 <= maximum; num1 += increment) {
                rowTable = new ArrayList<>();
                for (double num2 = minimum; num2 <= maximum; num2 += increment) {
                    if (getDoubleFromBigDecimal(BigDecimal.valueOf(num1).multiply(BigDecimal.valueOf(num2))) != null) {
                        rowTable.add(num1 * num2);
                    } else
                        rowTable.add(null);
                }
                multiplicationTable.add(rowTable);
                if (getDoubleFromBigDecimal(BigDecimal.valueOf(num1).add(BigDecimal.valueOf(increment))) == null) {
                    break;
                }
            }
        } else {
            for (double num1 = maximum; num1 >= minimum; num1 = num1 + increment) {
                rowTable = new ArrayList<>();
                for (double num2 = maximum; num2 >= minimum; num2 = num2 + increment) {
                    if (getDoubleFromBigDecimal(BigDecimal.valueOf(num1).multiply(BigDecimal.valueOf(num2))) != null) {
                        rowTable.add(num1 * num2);
                    } else
                        rowTable.add(null);
                }
                multiplicationTable.add(rowTable);
                if (getDoubleFromBigDecimal(BigDecimal.valueOf(num1).add(BigDecimal.valueOf(increment))) == null) {
                    break;
                }
            }
        }

        return multiplicationTable;
    }

    private Double getDoubleFromBigDecimal(BigDecimal value) {
        try {
            double number = Double.parseDouble(value.toString());

            if (number == Double.POSITIVE_INFINITY || number == Double.NEGATIVE_INFINITY
                    || ("byte".equals(getType()) && (number > Byte.MAX_VALUE || number < Byte.MIN_VALUE)
                    || ("short".equals(getType()) && (number > Short.MAX_VALUE || number < Short.MIN_VALUE))
                    || ("int".equals(getType()) && (number > Integer.MAX_VALUE || number < Integer.MIN_VALUE))
                    || ("long".equals(getType()) && (number > Long.MAX_VALUE || number < Long.MIN_VALUE))
                    || ("float".equals(getType()) && (number > Float.MAX_VALUE || number < -Float.MAX_VALUE)))) {
                return null;
            }
            return number;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    protected Number getNumber(String value) {
        Objects.requireNonNull(value);
        Number number = null;
        if ("byte".equals(getType())) number = getByte(value);
        if ("short".equals(getType())) number = getShort(value);
        if ("int".equals(getType())) number = getInt(value);
        if ("long".equals(getType())) number = getLong(value);
        if ("float".equals(getType())) number = getFloat(value);
        if ("double".equals(getType())) number = getDouble(value);

        return number;
    }

    private Double getDouble(String value) {
        Objects.requireNonNull(value);
        try {
            double number = Double.parseDouble(value);
            if (number == Double.POSITIVE_INFINITY || number == Double.NEGATIVE_INFINITY) {
                if (value.startsWith("-")) {
                    throw new IllegalArgumentException("The input value \"" + value
                            + "\" is less than a valid negative value of the type double");
                } else
                    throw new IllegalArgumentException("The input value \"" + value
                            + "\" is greater than a valid positive value of the type double");
            }

            return number;
        }catch (NumberFormatException e){
            throw new RuntimeException("Incorrect value \"" + value + "\" for type double", e);
        }
    }

    private Float getFloat(String value) {
        Objects.requireNonNull(value);
        try {
            float number = Float.parseFloat(value);
            if (number == Float.POSITIVE_INFINITY || number == Float.NEGATIVE_INFINITY) {
                if (value.startsWith("-")) {
                    throw new IllegalArgumentException("The input value \"" + value
                            + "\" is less than a valid negative value of the type float");
                } else
                    throw new IllegalArgumentException("The input value \"" + value
                            + "\" is greater than a valid positive value of the type float");
            }

            return number;
        }catch (NumberFormatException e){
            throw new RuntimeException("Incorrect value \"" + value + "\" for type float", e);
        }
    }

    private Long getLong(String value) {
        Objects.requireNonNull(value);
        Long number = null;
        try {
            number = Long.parseLong(value);
        } catch (NumberFormatException e) {
            throwExceptionForInputFormat(value);
            throwExceptionForOutputLimit(value);
        }
        return number;
    }

    private Integer getInt(String value) {
        Objects.requireNonNull(value);
        Integer number = null;
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throwExceptionForInputFormat(value);
            throwExceptionForOutputLimit(value);
        }
        return number;
    }

    protected Short getShort(String value) {
        Objects.requireNonNull(value);
        Short number = null;
        try {
            return Short.parseShort(value);
        } catch (NumberFormatException e) {
            throwExceptionForInputFormat(value);
            throwExceptionForOutputLimit(value);
        }
        return number;
    }

    private Byte getByte(String value) {
        Objects.requireNonNull(value);
        Byte number = null;
        try {
            number = Byte.parseByte(value);
        } catch (NumberFormatException e) {
            throwExceptionForInputFormat(value);
            throwExceptionForOutputLimit(value);
        }
        return number;
    }

    private void throwExceptionForOutputLimit(String value) {
        if (value.startsWith("-")) {
            throw new NumberFormatException("The input value \"" + value
                    + "\" is less than a valid negative value of the type " + getType());
        } else
            throw new NumberFormatException("The input value \"" + value
                    + "\" is greater than a valid positive value of the type " + getType());
    }

    private void throwExceptionForInputFormat(String value) {
        if (!value.matches("[-+]?\\d+")) {
            throw new NumberFormatException("Incorrect value \"" + value + "\" for type " + getType());
        }
    }

    private String getMultiplicationTableAsString(List<List<Number>> multiplicationTable) {
        StringBuilder result = new StringBuilder();
        for (List<Number> rowMultiplicationTable : multiplicationTable) {
            for (Number columnMultiplicationTable : rowMultiplicationTable) {
                if (columnMultiplicationTable == null) {
                    result.append("boom!").append(" ");
                    continue;
                }
                if ("float".equals(getType()) || "double".equals(getType())) {
                    result.append(columnMultiplicationTable.doubleValue()).append(" ");
                } else {
                    result.append(columnMultiplicationTable.longValue()).append(" ");
                }
            }
            result.append("\n");
        }
        return result.toString().trim();
    }

    private void checkInputValue(Number minimum, Number maximum, Number increment) {
        if (compareNumbers(minimum.doubleValue(), maximum.doubleValue()) == 0) {
            throw new IllegalArgumentException("Incorrect input min = max - iterations are impossible.");
        }
        if (compareNumberToZero(increment) == 0) {
            throw new IllegalArgumentException("Increment cannot be equals to '0'.");
        }
        if (compareNumbers(minimum.doubleValue(), maximum.doubleValue()) > 0) {
            log.info("!!! min = {} greater max = {}, so swap these values", minimum, maximum);
        }
    }

    private <T extends Number> int compareNumberToZero(T number) {
        double value = number.doubleValue();
        return Double.compare(value, 0.0);
    }

    private <T extends Number & Comparable<T>> int compareNumbers(T num1, T num2) {
        return num1.compareTo(num2);
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}

