package com.mkravchenko.util;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PropertiesUtilTest {

    @Test
    void getPropertyFromFileProperties() throws IOException {
        Properties properties = new Properties();
        var stream = PropertiesUtilTest.class.getClassLoader().getResourceAsStream("app.properties");
        properties.load(stream);
        Objects.requireNonNull(stream);
        stream.close();
        assertThat(PropertiesUtil.get("max"))
                .isEqualTo(properties.getProperty("max"));
    }

    @Test
    void throwExceptionIfFileNotFound() {
        assertThatThrownBy(() ->
                PropertiesUtil.class.getDeclaredMethod("loadProperties")
                        .invoke(null))
                .isInstanceOf(IllegalAccessException.class);
    }

}