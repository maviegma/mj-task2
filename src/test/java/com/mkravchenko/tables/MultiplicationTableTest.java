package com.mkravchenko.tables;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MultiplicationTableTest {

    private MultiplicationTable multiplicationTable;

    @BeforeAll
    void init() {
        multiplicationTable = new MultiplicationTable();
    }

    @ParameterizedTest
    @MethodSource("getByteInputArguments")
    void getByteMultiplicationTableAsString(String min, String max, String inc, String result) {
        multiplicationTable.setType("byte");
        assertThat(multiplicationTable.getStingResult(min, max, inc))
                .isEqualTo(result);
    }

    Stream<Arguments> getByteInputArguments() {
        return Stream.of(
                Arguments.of("1", "3", "1",
                        "1 2 3 \n" +
                                "2 4 6 \n" +
                                "3 6 9"
                ),
                Arguments.of("0", "3", "1",
                        "0 0 0 0 \n" +
                                "0 1 2 3 \n" +
                                "0 2 4 6 \n" +
                                "0 3 6 9"
                ),
                Arguments.of("1", "3", "-1",
                        "9 6 3 \n" +
                                "6 4 2 \n" +
                                "3 2 1"
                ),
                Arguments.of("-1", "-3", "-1",
                        "1 2 3 \n" +
                                "2 4 6 \n" +
                                "3 6 9"
                ),
                Arguments.of("-1", "3", "1",
                        "1 0 -1 -2 -3 \n" +
                                "0 0 0 0 0 \n" +
                                "-1 0 1 2 3 \n" +
                                "-2 0 2 4 6 \n" +
                                "-3 0 3 6 9"
                ),
                Arguments.of("1", "20", "5",
                        "1 6 11 16 \n" +
                                "6 36 66 96 \n" +
                                "11 66 121 boom! \n" +
                                "16 96 boom! boom!"
                ),
                Arguments.of("1", "20", "-5",
                        "boom! boom! boom! 100 \n" +
                                "boom! boom! boom! 75 \n" +
                                "boom! boom! 100 50 \n" +
                                "100 75 50 25"
                ),
                Arguments.of("2", "127", "126", "4")
        );
    }

    @ParameterizedTest
    @MethodSource("getShortInputArguments")
    void getShortMultiplicationTableAsString(String min, String max, String inc, String result) {
        multiplicationTable.setType("short");
        assertThat(multiplicationTable.getStingResult(min, max, inc))
                .isEqualTo(result);
    }

    Stream<Arguments> getShortInputArguments() {
        return Stream.of(
                Arguments.of("1", "3", "1",
                        "1 2 3 \n" +
                                "2 4 6 \n" +
                                "3 6 9"
                ),
                Arguments.of("0", "3", "1",
                        "0 0 0 0 \n" +
                                "0 1 2 3 \n" +
                                "0 2 4 6 \n" +
                                "0 3 6 9"
                ),
                Arguments.of("1", "3", "-1",
                        "9 6 3 \n" +
                                "6 4 2 \n" +
                                "3 2 1"
                ),
                Arguments.of("-1", "-3", "-1",
                        "1 2 3 \n" +
                                "2 4 6 \n" +
                                "3 6 9"
                ),
                Arguments.of("-1", "3", "1",
                        "1 0 -1 -2 -3 \n" +
                                "0 0 0 0 0 \n" +
                                "-1 0 1 2 3 \n" +
                                "-2 0 2 4 6 \n" +
                                "-3 0 3 6 9"
                ),
                Arguments.of("1", "20", "5",
                        "1 6 11 16 \n" +
                                "6 36 66 96 \n" +
                                "11 66 121 176 \n" +
                                "16 96 176 256"
                ),
                Arguments.of("1", "20", "-5",
                        "400 300 200 100 \n" +
                                "300 225 150 75 \n" +
                                "200 150 100 50 \n" +
                                "100 75 50 25"
                ), Arguments.of("180", "182", "1",
                        "32400 32580 32760 \n" +
                                "32580 32761 boom! \n" +
                                "32760 boom! boom!"
                ),
                Arguments.of("6", "32767", "32762", "36")
        );
    }

    @ParameterizedTest
    @MethodSource("getIntegerInputArguments")
    void getIntegerMultiplicationTableAsString(String min, String max, String inc, String result) {
        multiplicationTable.setType("int");
        assertThat(multiplicationTable.getStingResult(min, max, inc))
                .isEqualTo(result);
    }

    Stream<Arguments> getIntegerInputArguments() {
        return Stream.of(
                Arguments.of("1", "3", "1",
                        "1 2 3 \n" +
                                "2 4 6 \n" +
                                "3 6 9"
                ),
                Arguments.of("0", "3", "1",
                        "0 0 0 0 \n" +
                                "0 1 2 3 \n" +
                                "0 2 4 6 \n" +
                                "0 3 6 9"
                ),
                Arguments.of("1", "3", "-1",
                        "9 6 3 \n" +
                                "6 4 2 \n" +
                                "3 2 1"
                ),
                Arguments.of("-1", "-3", "-1",
                        "1 2 3 \n" +
                                "2 4 6 \n" +
                                "3 6 9"
                ),
                Arguments.of("-1", "3", "1",
                        "1 0 -1 -2 -3 \n" +
                                "0 0 0 0 0 \n" +
                                "-1 0 1 2 3 \n" +
                                "-2 0 2 4 6 \n" +
                                "-3 0 3 6 9"
                ),
                Arguments.of("1", "20", "5",
                        "1 6 11 16 \n" +
                                "6 36 66 96 \n" +
                                "11 66 121 176 \n" +
                                "16 96 176 256"
                ),
                Arguments.of("1", "20", "-5",
                        "400 300 200 100 \n" +
                                "300 225 150 75 \n" +
                                "200 150 100 50 \n" +
                                "100 75 50 25"
                ), Arguments.of("46339", "46342", "1",
                        "2147302921 2147349260 2147395599 2147441938 \n" +
                                "2147349260 2147395600 2147441940 boom! \n" +
                                "2147395599 2147441940 boom! boom! \n" +
                                "2147441938 boom! boom! boom!"
                ),
                Arguments.of("5", "2147483647", "2147483647", "25")
        );
    }

    @ParameterizedTest
    @MethodSource("getLongInputArguments")
    void getLongMultiplicationTableAsString(String min, String max, String inc, String result) {
        multiplicationTable.setType("long");
        assertThat(multiplicationTable.getStingResult(min, max, inc))
                .isEqualTo(result);
    }

    Stream<Arguments> getLongInputArguments() {
        return Stream.of(
                Arguments.of("-1", "3", "1",
                        "1 0 -1 -2 -3 \n" +
                                "0 0 0 0 0 \n" +
                                "-1 0 1 2 3 \n" +
                                "-2 0 2 4 6 \n" +
                                "-3 0 3 6 9"
                ),
                Arguments.of("1", "20", "5",
                        "1 6 11 16 \n" +
                                "6 36 66 96 \n" +
                                "11 66 121 176 \n" +
                                "16 96 176 256"
                ),
                Arguments.of("1", "20", "-5",
                        "400 300 200 100 \n" +
                                "300 225 150 75 \n" +
                                "200 150 100 50 \n" +
                                "100 75 50 25"
                ), Arguments.of("3037000499", "3037000500", "1",
                        "9223372030926248960 9223372033963249664 \n" +
                                "9223372033963249664 boom!"
                ),
                Arguments.of("2", "9223372036854775807", "9223372036854775807",
                        "4 boom! \n" +
                                "boom! boom!")
        );
    }

    @ParameterizedTest
    @MethodSource("getFloatInputArguments")
    void getFloatMultiplicationTableAsString(String min, String max, String inc, String result) {
        multiplicationTable.setType("float");
        assertThat(multiplicationTable.getStingResult(min, max, inc))
                .isEqualTo(result);
    }

    Stream<Arguments> getFloatInputArguments() {
        return Stream.of(
                Arguments.of("0.1", "0.13", "0.01",
                        getFloatMultiplyTableByPositiveIncrement("0.1", "0.13", "0.01")
                ),
                Arguments.of("-0.1", "-0.13", "0.02",
                        getFloatMultiplyTableByPositiveIncrement("-0.13", "-0.1", "0.02")
                ),
                Arguments.of("-0.1", "-0.13", "-0.01",
                        getFloatMultiplyTableByNegativeIncrement("-0.13", "-0.1", "-0.01")
                ),
                Arguments.of("0.1", "0.13", "-0.02",
                        getFloatMultiplyTableByNegativeIncrement("0.1", "0.13", "-0.02")
                )
        );
    }

    private String getFloatMultiplyTableByPositiveIncrement(String min, String max, String inc) {
        double minimum = Float.parseFloat(min);
        double maximum = Float.parseFloat(max);
        double increment = Float.parseFloat(inc);
        StringBuilder result = new StringBuilder();
        for (double i = minimum; i <= maximum; i += increment) {
            for (double j = minimum; j <= maximum; j += increment) {
                result.append(i * j).append(" ");
            }
            result.append("\n");
        }
        return result.toString().trim();
    }

    private String getFloatMultiplyTableByNegativeIncrement(String min, String max, String inc) {
        double minimum = Float.parseFloat(min);
        double maximum = Float.parseFloat(max);
        double increment = Float.parseFloat(inc);
        StringBuilder result = new StringBuilder();
        for (double i = maximum; i >= minimum; i += increment) {
            for (double j = maximum; j >= minimum; j += increment) {
                result.append(i * j).append(" ");
            }
            result.append("\n");
        }
        return result.toString().trim();
    }

    @ParameterizedTest
    @MethodSource("getDoubleInputArguments")
    void getDoubleMultiplicationTableAsString(String min, String max, String inc, String result) {
        multiplicationTable.setType("double");
        assertThat(multiplicationTable.getStingResult(min, max, inc))
                .isEqualTo(result);
    }

    Stream<Arguments> getDoubleInputArguments() {
        return Stream.of(
                Arguments.of("0.1", "0.13", "0.01",
                        getDoubleMultiplyTableByPositiveIncrement("0.1", "0.13", "0.01")
                ),
                Arguments.of("-0.1", "-0.13", "0.01",
                        getDoubleMultiplyTableByPositiveIncrement("-0.13", "-0.1", "0.01")
                ),
                Arguments.of("-0.1", "-0.13", "-0.01",
                        getDoubleMultiplyTableByNegativeIncrement("-0.13", "-0.1", "-0.01")
                ),
                Arguments.of("0.1", "0.13", "-0.02",
                        getDoubleMultiplyTableByNegativeIncrement("0.1", "0.13", "-0.02")
                ),
                Arguments.of("13", "15", "1",
                        getDoubleMultiplyTableByPositiveIncrement("13", "15", "1")
                )
        );
    }

    private String getDoubleMultiplyTableByPositiveIncrement(String min, String max, String inc) {
        double minimum = Double.parseDouble(min);
        double maximum = Double.parseDouble(max);
        double increment = Double.parseDouble(inc);
        StringBuilder result = new StringBuilder();
        for (double i = minimum; i <= maximum; i += increment) {
            for (double j = minimum; j <= maximum; j += increment) {
                result.append(i * j).append(" ");
            }
            result.append("\n");
        }
        return result.toString().trim();
    }

    private String getDoubleMultiplyTableByNegativeIncrement(String min, String max, String inc) {
        double minimum = Double.parseDouble(min);
        double maximum = Double.parseDouble(max);
        double increment = Double.parseDouble(inc);
        StringBuilder result = new StringBuilder();
        for (double i = maximum; i >= minimum; i += increment) {
            for (double j = maximum; j >= minimum; j += increment) {
                result.append(i * j).append(" ");
            }
            result.append("\n");
        }
        return result.toString().trim();
    }

    @ParameterizedTest
    @MethodSource("getInputArgumentsWithIncrementZero")
    void throwExceptionIfIncrementEqualsZero(String min, String max, String inc, String type) {
        multiplicationTable.setType(type);
        assertThatThrownBy(() -> multiplicationTable.getStingResult(min, max, inc))
                .isInstanceOf(Exception.class)
                .hasMessage("Increment cannot be equals to '0'.");
    }

    Stream<Arguments> getInputArgumentsWithIncrementZero() {
        return Stream.of(
                Arguments.of("1", "5", "0", "byte"),
                Arguments.of("1", "5", "0", "short"),
                Arguments.of("1", "5", "0", "int"),
                Arguments.of("1", "5", "0", "long"),
                Arguments.of("0.1", "0.5", "0.0", "float"),
                Arguments.of("1.0", "5.0", "0.0", "double")

        );
    }

    @ParameterizedTest
    @MethodSource("getInputArgumentsWhenMinEqualsMax")
    void throwExceptionWhenMinEqualsMax(String min, String max, String inc, String type) {
        multiplicationTable.setType(type);
        assertThatThrownBy(() -> multiplicationTable.getStingResult(min, max, inc))
                .isInstanceOf(Exception.class)
                .hasMessage("Incorrect input min = max - iterations are impossible.");
    }

    Stream<Arguments> getInputArgumentsWhenMinEqualsMax() {
        return Stream.of(
                Arguments.of("5", "5", "2", "byte"),
                Arguments.of("1", "1", "1", "short"),
                Arguments.of("10", "10", "1", "int"),
                Arguments.of("5", "5", "1", "long"),
                Arguments.of("0.1", "0.1", "0.01", "float"),
                Arguments.of("5.0", "5.0", "0.01", "double")
        );
    }

    @ParameterizedTest
    @MethodSource("getIncorrectInputValue")
    void throwExceptionIfIncorrectInput(String type, String value, String message) {
        multiplicationTable.setType(type);
        assertThatThrownBy(() -> multiplicationTable.getNumber(value))
                .isInstanceOf(Exception.class)
                .hasMessage(message);
    }

    Stream<Arguments> getIncorrectInputValue() {
        return Stream.of(
                Arguments.of("byte", null, null),
                Arguments.of("byte", "", "Incorrect value \"\" for type byte"),
                Arguments.of("byte", "10,4", "Incorrect value \"10,4\" for type byte"),
                Arguments.of("byte", "1..4", "Incorrect value \"1..4\" for type byte"),
                Arguments.of("byte", "1_4d", "Incorrect value \"1_4d\" for type byte"),
                Arguments.of("byte", "0.5", "Incorrect value \"0.5\" for type byte"),
                Arguments.of("byte", "abc", "Incorrect value \"abc\" for type byte"),
                Arguments.of("byte", "128", "The input value \"128\"" +
                        " is greater than a valid positive value of the type byte"),
                Arguments.of("byte", "-129", "The input value \"-129\"" +
                        " is less than a valid negative value of the type byte"),
                Arguments.of("short", null, null),
                Arguments.of("short", "", "Incorrect value \"\" for type short"),
                Arguments.of("short", "10,4", "Incorrect value \"10,4\" for type short"),
                Arguments.of("short", "1..4", "Incorrect value \"1..4\" for type short"),
                Arguments.of("short", "1_4d", "Incorrect value \"1_4d\" for type short"),
                Arguments.of("short", "0.5", "Incorrect value \"0.5\" for type short"),
                Arguments.of("short", "abc", "Incorrect value \"abc\" for type short"),
                Arguments.of("short", "32768", "The input value \"32768\"" +
                        " is greater than a valid positive value of the type short"),
                Arguments.of("short", "-32769", "The input value \"-32769\"" +
                        " is less than a valid negative value of the type short"),
                Arguments.of("int", null, null),
                Arguments.of("int", "", "Incorrect value \"\" for type int"),
                Arguments.of("int", "10,4", "Incorrect value \"10,4\" for type int"),
                Arguments.of("int", "1..4", "Incorrect value \"1..4\" for type int"),
                Arguments.of("int", "1_4d", "Incorrect value \"1_4d\" for type int"),
                Arguments.of("int", "0.5", "Incorrect value \"0.5\" for type int"),
                Arguments.of("int", "abc", "Incorrect value \"abc\" for type int"),
                Arguments.of("int", "2147483648", "The input value \"2147483648\" " +
                        "is greater than a valid positive value of the type int"),
                Arguments.of("int", "-2147483649", "The input value \"-2147483649\"" +
                        " is less than a valid negative value of the type int"),
                Arguments.of("long", null, null),
                Arguments.of("long", "", "Incorrect value \"\" for type long"),
                Arguments.of("long", "10,4", "Incorrect value \"10,4\" for type long"),
                Arguments.of("long", "1..4", "Incorrect value \"1..4\" for type long"),
                Arguments.of("long", "1_4d", "Incorrect value \"1_4d\" for type long"),
                Arguments.of("long", "0.5", "Incorrect value \"0.5\" for type long"),
                Arguments.of("long", "abc", "Incorrect value \"abc\" for type long"),
                Arguments.of("long", "9223372036854775808", "The input value \"9223372036854775808\"" +
                        " is greater than a valid positive value of the type long"),
                Arguments.of("long", "-9223372036854775809", "The input value \"-9223372036854775809\"" +
                        " is less than a valid negative value of the type long"),
                Arguments.of("float", null, null),
                Arguments.of("float", "", "Incorrect value \"\" for type float"),
                Arguments.of("float", "10,4", "Incorrect value \"10,4\" for type float"),
                Arguments.of("float", "1..4", "Incorrect value \"1..4\" for type float"),
                Arguments.of("float", "1_4d", "Incorrect value \"1_4d\" for type float"),
                Arguments.of("float", "abc", "Incorrect value \"abc\" for type float"),
                Arguments.of("float", "3.4028235E39", "The input value \"3.4028235E39\"" +
                        " is greater than a valid positive value of the type float"),
                Arguments.of("float", "-3.4028235E39", "The input value \"-3.4028235E39\"" +
                        " is less than a valid negative value of the type float"),
                Arguments.of("double", null, null),
                Arguments.of("double", "", "Incorrect value \"\" for type double"),
                Arguments.of("double", "10,4", "Incorrect value \"10,4\" for type double"),
                Arguments.of("double", "1..4", "Incorrect value \"1..4\" for type double"),
                Arguments.of("double", "1_4d", "Incorrect value \"1_4d\" for type double"),
                Arguments.of("double", "abc", "Incorrect value \"abc\" for type double"),
                Arguments.of("double", "1.7976931348623157E309", "The input value \"1.7976931348623157E309\"" +
                        " is greater than a valid positive value of the type double"),
                Arguments.of("double", "-1.7976931348623157E309", "The input value \"-1.7976931348623157E309\"" +
                        " is less than a valid negative value of the type double")
        );
    }
}