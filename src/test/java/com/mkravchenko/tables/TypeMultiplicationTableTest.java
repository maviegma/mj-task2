package com.mkravchenko.tables;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TypeMultiplicationTableTest {

    @ParameterizedTest
    @MethodSource("getTypeAndClassMultiplyTableName")
    void getMultiplyTableByType(String type) {
        assertThat(TypeMultiplicationTable.getMultiplicationTable(type).getClass())
                .isEqualTo(MultiplicationTable.class);
    }

    Stream<Arguments> getTypeAndClassMultiplyTableName() {
        return Stream.of(
                Arguments.of("int"),
                Arguments.of("byte"),
                Arguments.of("short"),
                Arguments.of("long"),
                Arguments.of("float"),
                Arguments.of("double")
        );
    }

    @ParameterizedTest
    @MethodSource("getInputAndOutputType")
    void getTypeFromMultiplyTable(String inputType, String outputType) {
        assertThat(TypeMultiplicationTable.getMultiplicationTable(inputType).getType())
                .isEqualTo(outputType);
    }

    Stream<Arguments> getInputAndOutputType() {
        return Stream.of(
                Arguments.of("int", "int"),
                Arguments.of("INT", "int"),
                Arguments.of("byte", "byte"),
                Arguments.of("BYTE", "byte"),
                Arguments.of("short", "short"),
                Arguments.of("SHORT", "short"),
                Arguments.of("long", "long"),
                Arguments.of("LONG", "long"),
                Arguments.of("float", "float"),
                Arguments.of("FLOAT", "float"),
                Arguments.of("double", "double"),
                Arguments.of("DOUBLE", "double")
        );
    }

    @Test
    void throwExceptionIfIncorrectInputType() {
        String type = "shor";
        assertThatThrownBy(() -> TypeMultiplicationTable.getMultiplicationTable(type))
                .isInstanceOf(Exception.class)
                .hasMessage("Incorrect number type name \"" + type
                        + "\". Select type byte, short, integer, long, float or double.");
    }

}